package com.telerikacademy.testframework;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
	public enum PropertiesManagerEnum {
		INSTANCE;
		private Properties configProperties = null;
		private Properties uiMappings = null;
		private static final String UI_MAP = "src/test/resources/mappings/ui_map.properties";
//		private static final String REG_MAP = "src/test/resources/mappings/registration_form.properties";
//		private static final String LOG_MAP = "src/test/resources/mappings/login_form.properties";
//		private static final String POST_MAP = "src/test/resources/mappings/post.properties";
//		private static final String PROFADMIN_MAP = "src/test/resources/mappings/profile_administration.properties";
		private static final String CONFIG_PROPERTIES = "src/test/resources/config.properties";


		private static Properties loadProperties(String url) {
			Properties props = new Properties();
			try {
				props.load(new FileInputStream(url));
			} catch (IOException ex){
				Utils.LOG.error("Loading properties failed!");
			}
			return props;
		}

		public Properties getConfigProperties() {
			return configProperties = loadProperties(CONFIG_PROPERTIES);
		}

		public Properties getRegistrationFormMappings() {
			return uiMappings = loadProperties(UI_MAP);
		}

		public Properties getUiMappings() { return uiMappings = loadProperties(UI_MAP); }

//		public Properties getLoginFormMappings() { return uiMappings = loadProperties(LOG_MAP); }
//		public Properties getPostMappings() { return uiMappings = loadProperties(POST_MAP); }
//		public Properties getProfileAdministrationMappings() { return uiMappings = loadProperties(PROFADMIN_MAP); }
	}
}

package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.BeforeStory;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.FindFailed;

import java.lang.annotation.Target;

public class BaseStepDefinitions {
    @BeforeStories
    public void beforeStories(){
        UserActions.loadBrowser();
    }

    //This is only for E2E Scenarios run
//    @BeforeStory
//    public void beforeStory() throws FindFailed { UserActions.logOut(); }

    @AfterStories
    public void afterStories(){
        UserActions.quitDriver();
    }
}

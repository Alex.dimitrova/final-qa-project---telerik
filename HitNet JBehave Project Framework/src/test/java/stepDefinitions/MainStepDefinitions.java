package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Aliases;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.GivenStory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import java.io.File;

public class MainStepDefinitions extends BaseStepDefinitions {
    private UserActions actions = new UserActions();
    private Screen screen = new Screen();

    /* MAIN STEP DEFINITIONS */
    @Given("Click $element {element|button|tab}")
    @When("Click $element {element|button|tab}")
    @Then("Click $element {element|button|tab}")
    public void clickElement(String element){
        UserActions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        String textValue = Utils.getConfigPropertyByKey(value);
        UserActions.typeValueInField(textValue, field);
    }

    @Given("Clear $field field")
    @When("Clear $field field")
    @Then("Clear $field field")
    public void clearField(String field){
        UserActions.clearField(field);
    }

    @Given("{Element|Button|Tab} $element is present")
    @When("{Element|Button|Tab} $element is present")
    @Then("{Element|Button|Tab} $element is present")
    public void elementsIsPresent(String element){
        UserActions.waitForElementVisible(element, 30);
    }

    @Given("{Element|Button|Tab} $element is not present")
    @When("{Element|Button|Tab} $element is not present")
    @Then("{Element|Button|Tab} $element is not present")
    public void elementsNotPresent(String locator){
        actions.assertElementNotPresent(locator);
    }


    /* REGISTRATION */
    @Given("Navigate to registration form")
    public void navigateToRegistrationForm() {
        String registrationButton = Utils.getUIMappingByKey("newUserButton");
        UserActions.navigateTo(registrationButton);
    }

    @When("I fill registration fields")
    public void fillRegistrationFields() {
        elementsIsPresent("signUpButton");
        typeInField("firstName", "firstNameField");
        typeInField("lastName", "lastNameField");
        typeInField("email", "emailRegistrationField");
        typeInField("password", "passwordField");
        typeInField("password", "confirmPasswordField");
        clickElement("signUpButton");
        Utils.LOG.info("Navigated to second next registration fields");
    }

    @When("I set my age")
    public void setAge() {
        elementsIsPresent("ageField");
        clearField("ageField");
        typeInField("age", "ageField");
    }

    @When("I sign up")
    @Then("I sign up")
    public void signUp() {
        UserActions.clickElement("createProfileButton");
        Utils.LOG.info("Registration button is clicked");
    }


    /* LOGIN */
    @Given("I have logged in")
    @When("I have logged in")
    public void LogIn() {
        typeInField("username", "emailLoginField");
        typeInField("password", "passwordLoginField");
        clickElement("loginButton");
        Utils.LOG.info("Login button clicked");
    }

    @Given("I have logged in as another user")
    @When("I have logged in as another user")
    public void Login() {
        clickElement("emailLoginField");
        typeInField("username2", "emailLoginField");
        typeInField("password", "passwordLoginField");
        clickElement("loginButton");
        Utils.LOG.info("Login button clicked");
    }

    @Then("I have logged out")
    @Aliases(values={"I log out"})
    public void logOut() {
        elementsIsPresent("optionsButton");
        clickElement("optionsButton");
        clickElement("logOutButton");
        Utils.LOG.info("LogOut button clicked");
    }


    /* REGISTERED USER HOMEPAGE */
    @When("I navigate to public feed")
    public void navigateToPublicFeed() {
        String publicFeed = Utils.getUIMappingByKey("publicFeed");
        UserActions.navigateTo(publicFeed);
    }


    /* POST CREATION, POST LIKING, POST COMMENTING */
    @Given("I make a post")
    @When("I make a post")
    public void createPost() {
        typeInField(Utils.getConfigPropertyByKey("postText"), "postField");
        clickElement("createPostButton");
        Utils.LOG.info("Post create button is clicked");
    }

    @When("Edit my post")
    public void editPost() throws FindFailed {

        elementsIsPresent("threeDotsButton");
        clickElement("threeDotsButton");

        String editPostButton = "src/test/resources/Images/editButton.PNG";
        screen.wait(editPostButton, 10);
        screen.click(editPostButton);

        clearField("editPostField");
        typeInField(Utils.getConfigPropertyByKey("editedPostText"), "editPostField");
        clickElement("savePostEdit");
    }

    @Then("Delete my post")
    public void deletePost() throws FindFailed {
        elementsIsPresent("threeDotsButton");
        clickElement("threeDotsButton");

        String deleteButton = "src/test/resources/Images/deleteButton.PNG";
        screen.wait(deleteButton, 10);
        screen.click(deleteButton);
    }

    @When("I like a post")
    public void likeAPost() throws FindFailed {
        String likeButton = "\\src\\test\\resources\\Images\\like_button.png";
        screen.wait(likeButton, 10);
        screen.click(likeButton);
    }

    @Then("Dislike button appears")
    public void dislikeButton(){
        String dislikeButton = "\\src\\test\\resources\\Images\\dislike_button.png";
        screen.exists(dislikeButton);
    }


    /* PROFILE ADMINISTRATION - EDIT */
    @When("Edit my profile")
    public void editProfile() {
        clickElement("profileAdministrationButton");
        clickElement("profileInfo");
        clickElement("profileEditInfo");

        clearField("firstNameEditField");
        typeInField("newFirstName", "firstNameEditField");
        clearField("lastNameEditField");
        typeInField("newLastName", "lastNameEditField");

        clearField("ageField");
        typeInField("newAge", "ageField");

        clickElement("saveProfileEditChanges");
    }

    @When("I upload a profile picture")
    @Then("I upload a profile picture")
    public void uploadPic(){
        File profilePic = new File("Images/profile_picture.png");
        String absolutePath = profilePic.getAbsolutePath();
        UserActions.uploadPhoto();
        screen.exists(absolutePath);
    }
}

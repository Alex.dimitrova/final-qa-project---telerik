Meta:
@publicPart

Narrative:
As a user
I want to be able to login with valid credentials

Scenario:
Given Element emailLoginField is present
When Click emailLoginField element
And Type username in emailLoginField field
And Type password in passwordLoginField field
And Click loginButton element
Then Element postField is present
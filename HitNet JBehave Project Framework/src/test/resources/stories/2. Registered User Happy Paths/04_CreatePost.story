Meta:
@regUsers

Narrative:
As a user
I want to create a post

Scenario:
Given I have logged in
And Element postField is present
When Click postField element
And Type postText in postField field
Then Click createPostButton element
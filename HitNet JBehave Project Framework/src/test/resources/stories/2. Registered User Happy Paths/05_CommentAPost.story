Meta:
@regUsers

Narrative:
As a user
I want to be able to comment on posts

Scenario:
Given Element publicFeed is present
When Click publicFeedTab element
And Element postCard is present
And Click addACommentField element
And Type comment in addACommentField field
And Click addAComment element
Then Element commentIsPosted is present
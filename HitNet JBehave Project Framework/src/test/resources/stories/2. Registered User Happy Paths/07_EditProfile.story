Meta:
@skip

Narrative:
As a user
I want edit my profile with all sections

Scenario:
Given Element profileAdministrationButton is present
Then Click profileAdministrationButton button
And Click profileInfo button
And Click profileEditInfo button
And Type newFirstName in firstNameEditField field
And Type newLastName in lastNameEditField field
And Type newAge in ageField field
And Type professionalNickname in professionalNicknameField field
And Type style in styleField field
And Type aboutMe in aboutMeField field
When Button saveProfileEditChanges is present
Then Click saveProfileEditChanges button

Meta:
@regUsers

Narrative:
As a user
I want to be able to send friend request
So that I can interact with other users

Scenario: Navigate to another user profile and send a friend request
When Click wallButton button
And Element newestMembersArea is present
And Click clickToViewProfile element
Then Button addAFriend is present
When Click addAFriend button
Then Button removeAFriend is present

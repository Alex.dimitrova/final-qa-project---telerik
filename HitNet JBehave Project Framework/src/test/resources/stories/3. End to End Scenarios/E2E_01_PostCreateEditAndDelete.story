Meta:
@skip

Narrative:
As a user
I want to make a registration
Make a post, edit it and then delete the post

Scenario: I make registration
Given Navigate to registration form
When I fill registration fields
And I set my age
And I sign up
Then I log out

Scenario: Make a post and edit profile
When I have logged in
When I make a post
And I navigate to public feed
And Edit my post
Then Delete my post
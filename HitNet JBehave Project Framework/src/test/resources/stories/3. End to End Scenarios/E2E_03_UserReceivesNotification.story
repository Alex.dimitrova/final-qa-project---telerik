Meta:
@e2e

Narrative:
As a user
I want to login, add friends, comments and like on another user's posts
then check if other user receives notification

Scenario: Navigate to another user's profile and like his post
Given I have logged in
When Element newestMembersArea is present
And Element clickToViewProfile is present
And Click clickToViewProfile element
When I like a post
Then Dislike button appears

Scenario: Comment on his post
Given Element postCard is present
When Type comment in addACommentField field
And Click addAComment element
Then Element commentIsPosted is present

Scenario: Send a friend request
Given Button addAFriend is present
When Click addAFriend button
Then Button removeAFriend is present
Then I log out

Scenario: Login as another user, open profile's landing page and check notifications
Given Element socialNetworkLogo is present
When I have logged in as another user
And Element publicFeedTab is present
When Click notificationsMenu tab
Then Element likeNotification is present
And Element commentNotification is present
And Element friendNotification is present



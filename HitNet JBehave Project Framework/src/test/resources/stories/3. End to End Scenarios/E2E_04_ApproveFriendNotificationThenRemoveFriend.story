Meta:
@e2e

Narrative:
As a registered user
I want to be able to approve friends requests
and then remove friend

Scenario: Check friends request and approve request
Given I have logged in as another user
When Tab notificationsMenu is present
When Click notificationsMenu tab
And Element friendNotification is present
When Click friendNotification element
Then Button acceptFriend is present
When Click acceptFriend button
Then Button friendship is present

Scenario: check friendship
Given Tab profile is present
When Click profile tab
And Tab friends is present
And Click friends tab
Then Element userFriends is present

Scenario: open profile and remove friend
Given Element userFriends is present
When Click friendProfile element
And Button friendship is present
And Click friendship button
When Click removeFriend button
Then Button friendship is not present
# HitNet Social Network Testing - Final QA Projet
Hitnet is the world’s ﬁrst (outside the darkweb) social network for professional and wannabe hitmen. It is a straight-forward and easy website to stay connected with your wannabe hitmen friends, family, colleagues, customers and so on.

The testing eﬀort of our QA team was focused on System level where end-to-end user scenarios was tested via functional and automation tests.
Testing is performed to expose defects in the interfaces and in the interactions between integrated components.

## Built With
* [TestRail+](https://www.gurock.com/testrail) - Test Case Management
* [Postman](https://www.getpostman.com/) - Used for integration tests
* [JBehave](hhttps://jbehave.org/) - BDD framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Newman](https://www.npmjs.com/package/newman) - Command-line collection runner for Postman
* [JMeter](https://jmeter.apache.org/) - Application used to test performance

## Resources
1. [Test Plan](https://docdro.id/Mw6TA8X)
2. [TestRail+ link for test cases](https://howtoqa.testrail.io/index.php?/projects/overview/6)
3. [Hitnet Social Network Repo](https://gitlab.com/dimasss/social-network-project)
4. [Logged issues due testing](https://docdro.id/5JV2Zzx)
5. [Test Report](https://docdro.id/IQoFEnI)

## Running the tests
- To execute integration tests the following packages have to be installed

1.   [Newman](https://www.npmjs.com/package/newman)
2.   [HTML reporter](https://www.npmjs.com/package/newman-reporter-htmlextra) 

- To execute the Jbehave tests Maven have to be installed
1.   [Maven](https://maven.apache.org/)

## Authors
* **Bilyana Hristova** - [gitlab](https://gitlab.com/bilyanahristova)
* **Alexadra Dimitrova** - [gitlab](https://gitlab.com/Alex.dimitrova)
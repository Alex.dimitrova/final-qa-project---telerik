@ECHO OFF

SET postman_collection=Comment.postman_collection.json
SET postman_environment=CommentURL.postman_environment.json
SET postman_globals=HitNet.postman_globals.json

call newman run %postman_collection% -r htmlextra,cli -e %postman_environment% -g %postman_globals% 

SET postman_collection=Friends.postman_collection.json
SET postman_environment=FriendsURL.postman_environment.json
SET postman_globals=HitNet.postman_globals.json

call newman run %postman_collection% -r htmlextra,cli -e %postman_environment% -g %postman_globals% 

SET postman_collection=Notification.postman_collection.json
SET postman_environment=NotificationURL.postman_environment.json
SET postman_globals=HitNet.postman_globals.json


call newman run %postman_collection% -r htmlextra,cli -e %postman_environment% -g %postman_globals% 

SET postman_collection=Post.postman_collection.json
SET postman_environment=PostURL.postman_environment.json
SET postman_globals=HitNet.postman_globals.json

call newman run %postman_collection% -r htmlextra,cli -e %postman_environment% -g %postman_globals%

SET postman_collection=User.postman_collection.json
SET postman_environment=UserURL.postman_environment.json
SET postman_globals=HitNet.postman_globals.json